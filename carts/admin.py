from django import forms
from django.contrib import admin
from .models import Cart, CartItem


class CartItemAdmin(admin.ModelAdmin):
    readonly_fields = ('total_price', )

    def total_price(self, obj):
        return obj.product.price * obj.quantity


# Register your models here.
admin.site.register(Cart)
admin.site.register(CartItem, CartItemAdmin)
