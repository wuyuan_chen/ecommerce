from rest_framework import serializers
from carts.models import Cart, CartItem
from products.models import Product
from django.contrib.auth.models import User


class CartSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.name')

    class Meta:
        model = Cart
        fields = ["id", "user", "created_at"]

    def create(self, validated_data):
        try:
            print(validated_data)
            username = validated_data['user'].get("name")
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            print(f"User not found during deserializing process: {username}")
            raise serializers.ValidationError("user not found.")
        cart = Cart.objects.get_or_create(user=user)
        return cart


class CartItemSerializer(serializers.ModelSerializer):
    product = serializers.CharField(source="product.id")

    class Meta:
        model = CartItem
        fields = ["id", "product", "quantity"]

    def create(self, data):
        request = self.context.get("request", None)
        print("request: ", request)
        if not request:
            print("no request")
            return False

        product = data.get("product")
        quantity = data.get("quantity")
        cart, is_created = Cart.objects.get_or_create(user=request.user)
        product = Product.objects.get(id=product.get("id"))
        cart_item, is_updated = CartItem.objects.update_or_create(
                cart=cart, product=product,
                defaults = {"quantity": quantity})
        return cart_item


