from django.urls import path

from . import views

app_name = "carts"
urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("savecart/", views.SaveCartView.as_view(), name="save"),
    path("checkout/", views.CheckoutView.as_view(), name="checkout"),
]
