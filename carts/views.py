import json
from django.shortcuts import get_object_or_404, redirect, render
from django.views import generic, View
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.urls import reverse_lazy

from .models import Cart, CartItem
from products.models import Product
from .serializers import CartItemSerializer, CartSerializer


class ListView(generic.ListView):
    model = Cart
    template_name = 'carts/cart_list.html'
    context_object_name = 'cart_list'

    def get_queryset(self):
        # Filter the queryset to get the cart for the current user
        # return get_object_or_404(Cart, user=self.request.user)

        try:
            cart = Cart.objects.filter(user=self.request.user)
            return cart[0]
        except TypeError:
            return Cart.objects.none()


class SaveCartView(View):
    def post(self, request):
        print("save cart view..", request.POST)
        if not request.user.is_authenticated:
            return HttpResponse(json.dumps({"result": "redirect", "status": 302}))

        cart = json.loads(request.POST.get("cart"))
        for item_id, cart_item in cart.items():
            print(cart_item)
            data = {
                "product": item_id,
                "quantity": cart_item.get("count"),
            }
            s = CartItemSerializer(data=data,
                                   context={"request": request})
            if s.is_valid():
                s.save()
            else:
                print(s.errors)

        return HttpResponse(json.dumps({"result": "save cart successful", "status": 201}))


class CheckoutView(View):
    template_name = "carts/checkout.html"

    def get(self, request):
        return render(request, self.template_name)
