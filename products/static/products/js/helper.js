$(document).ready(function() {
    "use strict";
    console.log("nav javascript")
    let countElement = $("#cart-item-count");
    countElement.text(shoppingCart.getCartCount()); //update the display

    // Listen for the custom event and update UI accordingly
    $(document).on('UpdateTotalCountEvent', function(event, data) {
        console.log('Custom event triggered with data:', data);
       countElement.text(shoppingCart.getCartCount()); //update the display
        // Update UI based on the data received
    });

});