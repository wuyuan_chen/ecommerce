let shoppingCart = (function() {
    // private methods and properties
    let cart = {};
    let cartCount = 0;

    function Item(id, name, price, count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    function saveCart() {
        localStorage.setItem("shoppingCart", JSON.stringify(cart));
    }

    function loadCart() {
        console.log("loadCart...")
        cart = JSON.parse(localStorage.getItem("shoppingCart"));
        if (cart === null) {
            cart = {}
        } else {
            // get the total items in the cart
            for (let i in cart) {
                cartCount += cart[i].count;
            }
        }
    }

    function roundToDecimal(num) {
        return Math.round(num * 100) /100;
    }


    loadCart();
    // public methods and properties

    let obj = {}
    obj.addItemToCart = function(id, name, price, count) {
        if (id in cart) {
            cart[id].count += count;
        } else {
            console.log("addItemToCart: ", id, name, price, count);

            let item = new Item(id, name, price, count)
            cart[id] = item;
        }
        cartCount += 1
        saveCart();
    }

    obj.removeItemFromCart = function(id) { // remove one item
        if (id in cart) {
            cart[id].count -= 1;
            cartCount -= 1;
            if (cart[id].count === 0) {
                delete cart[id];
            }
            saveCart();
        }
    }

    obj.removeProductFromCart = function(id) {
        if (id in cart) {
            cartCount -= cart[id].count;
            delete cart[id];
            saveCart()
        }
    }

    obj.listItemInCart = function() {
        // return new copys so it doesn't
        // mess up with the original
        let cartCopy = {};
        for (let i in cart) {
            itemCopy = {}
            for (let k in cart[i]) {
                itemCopy[k] = cart[i][k]
            }
            cartCopy[i] = itemCopy;
        }
        return cartCopy;
    }

    obj.getCartCount = function() {
        return cartCount;
    }

    obj.updateItemCount = function(id, count) { 
        console.log(typeof id)
        if (id in cart) {
            cart[id].count += count;
            cartCount += count;
            if (cart[id].count === 0) {
                delete cart[id];
            }
            saveCart();
        }
    }

    obj.getItemTotalCost = function(id) {
        if (id in cart) {
            return roundToDecimal(cart[id].count * cart[id].price);
        }
        return 0
    }

    obj.getTotalCartCost = function() {
        let total = 0;
        for (let i in cart) {
            const item = cart[i];
            const itemCost = this.getItemTotalCost(i);
            total += itemCost;
        }
        return roundToDecimal(total);
    }

    return obj;
})()