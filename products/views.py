from django.db.models import F
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic

from .models import Product

# Create your views here.


class ListView(generic.ListView):
    # model = Product
    queryset = Product.objects.select_related("user")


class DetailView(generic.DetailView):
    model = Product
