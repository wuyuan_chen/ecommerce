from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LogoutView
from .forms import UserRegisterForm
from django.contrib import messages

# Create your views here.


# https://youtu.be/q4jPR-M0TAQ?si=I9fg_LdpEZnnxFLS&t=1644
def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request,
                "Your Account has been created! You are now able to login.")
            return redirect("login")
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})


class UserLogoutView(LogoutView):

    def get(self, request):
        logout(request)
        return redirect('login')


@login_required
def profile(request):
    return render(request, 'users/profile.html')
